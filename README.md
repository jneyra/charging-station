# Requirements
* Java 8
* Gradle (included as part of the project)
* Spring Boot 2.1.5 (dependencies are downloaded via Gradle)

# Running 
```    
./gradlew bootRun
```
* It will start a web server in to the port 8080
* The base URL will be: `http://localhost:8080`

# Approach

* The data is stored in a `ConcurrentMap`
* There were create 2 indexes based on status (`IN_PROGRESS` and `FINISHED`)
* The AVL Tree is used to store de indexes.
* Each node in the AVL Tree holds the `pre-calculated weight`
* The session summary calculates the totals based on the `weight`

# REST end-points

## POST chargingSessions

* Submit a charging new charging session for the station

### Request Body
```json
{
  "stationId": "ABC-12345",
  "timestamp": "2019-05-06T19:00:20.122"
}
```

### Response Body
```json
{
  "id": "9dfd9212-9089-a09d-9009-565a56c56",
  "stationId": "ABC-12345",
  "timestamp": "2019-05-06T19:00:20.122"
}
```

### Response Status
* CREATED (202): The session was created.

## PUT chargingSessions/{id}
* Stop the charging session with `id`

### Response Body
```json
{
  "id": "9dfd9212-9089-a09d-9009-565a56c56",
  "stationId": "ABC-12345",
  "startedAt": "2019-05-06T19:00:20.122",
  "status": "FINISHED"
}
```

### Response Status
* ACCEPTED (202): The session was stopped.
* NOT_MODIFIED (304): The session is already stopped.
* NOT_FOUND (402): The session was not found.

## GET chargingSessions
* List all the charging sessions

### Response Body
```json
[
    {
      "id": "9dfd9212-9089-a09d-9009-565a56c56",
      "stationId": "ABC-12345",
      "startedAt": "2019-05-06T19:00:20.122",
      "status": "IN_PROGRESS"
    },
    {
      "id": "a897a7878-a09d-9009-9089-ba89a7989",
      "stationId": "ABC-12346",
      "startedAt": "2019-05-06T19:00:20.122",
      "status": "FINISHED"
    }
]

```

### Response Status
* OK (200)

## GET chargingSessions/summary
* Retrieve a summary of the last submitted charging sessions.

### Response Body
```json
{
  "totalCount": 5,
  "startedCount": 3,
  "stoppedCount": 2,
}
```
### Response Status
* OK (200)
