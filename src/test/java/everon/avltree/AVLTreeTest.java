package everon.avltree;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AVLTreeTest {
  AVLTree<Integer, Integer> tree = new AVLTree<>();

  @Before
  public void prepare() {
    tree.clear();
  }

  @Test
  public void testTree() {
    for (int i = 0; i < 100; i++) {
      tree.insert(i, i);
    }
    assertEquals(100, tree.size());
    assertEquals(95, tree.countGreaterOrEquals(5));

    tree.insert(101, 101);
    assertEquals(101, tree.size());
    assertEquals(51, tree.countGreaterOrEquals(50));

    tree.remove(10);
    assertEquals(95, tree.countGreaterOrEquals(5));
    assertEquals(51, tree.countGreaterOrEquals(50));

    tree.remove(20);
    assertEquals(94, tree.countGreaterOrEquals(5));
    assertEquals(51, tree.countGreaterOrEquals(50));

    tree.remove(30);
    assertEquals(93, tree.countGreaterOrEquals(5));
    assertEquals(51, tree.countGreaterOrEquals(50));

    tree.remove(40);
    assertEquals(92, tree.countGreaterOrEquals(5));
    assertEquals(51, tree.countGreaterOrEquals(50));

    tree.remove(50);
    assertEquals(91, tree.countGreaterOrEquals(5));
    assertEquals(50, tree.countGreaterOrEquals(50));
    assertEquals(96, tree.size());
  }
}
