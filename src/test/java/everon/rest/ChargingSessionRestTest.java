package everon.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import everon.model.ChargingSession;
import everon.model.ChargingSessionCreate;
import everon.model.SessionSummary;
import everon.model.StatusEnum;
import everon.repository.ChargingSessionRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ChargingSessionRest.class)
public class ChargingSessionRestTest {
  @Autowired
  private MockMvc mvc;

  @Autowired
  private ObjectMapper objectMapper;

  @MockBean
  private ChargingSessionRepository repository;

  private DateTimeFormatter format = DateTimeFormatter.ISO_DATE_TIME;

  @Test
  public void testInsert() throws Exception {

    ChargingSessionCreate create = new ChargingSessionCreate();
    create.setStationId("STATION-123");
    create.setTimestamp(format.format(LocalDateTime.now()));
    String jsonBody = objectMapper.writeValueAsString(create);

    ChargingSession session = new ChargingSession();
    session.setId(UUID.randomUUID());

    when(repository.insert(any(ChargingSession.class))).thenReturn(session);

    mvc.perform(post("/chargingSessions").content(jsonBody).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());

    verify(repository, times(1)).insert(any(ChargingSession.class));
  }

  @Test
  public void testUpdate_ok() throws Exception {

    ChargingSessionCreate create = new ChargingSessionCreate();
    create.setStationId("STATION-123");
    create.setTimestamp(format.format(LocalDateTime.now()));
    String jsonBody = objectMapper.writeValueAsString(create);
    UUID uuid = UUID.randomUUID();

    ChargingSession session = new ChargingSession();
    session.setStatus(StatusEnum.IN_PROGRESS);
    session.setId(uuid);

    when(repository.getById(uuid.toString())).thenReturn(session);

    mvc.perform(put("/chargingSessions/{id}", uuid).content(jsonBody).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isAccepted());

    verify(repository, times(1)).update(any(ChargingSession.class));
  }

  @Test
  public void testUpdate_doesnt_exist() throws Exception {
    UUID uuid = UUID.randomUUID();

    when(repository.getById(anyString())).thenReturn(null);

    mvc.perform(put("/chargingSessions/{id}", uuid).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());

    verify(repository, times(0)).update(any(ChargingSession.class));
  }

  @Test
  public void testUpdate_already_finished() throws Exception {
    UUID uuid = UUID.randomUUID();

    ChargingSession session = new ChargingSession();
    session.setStatus(StatusEnum.FINISHED);
    session.setId(uuid);

    when(repository.getById(anyString())).thenReturn(session);

    mvc.perform(put("/chargingSessions/{id}", uuid).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotModified());

    verify(repository, times(0)).update(any(ChargingSession.class));
  }

  @Test
  public void testListAll() throws Exception {
    List<ChargingSession> sessions = Arrays.asList(new ChargingSession() {{
      setId(UUID.randomUUID());
      setStationId("STATION-1");
      setStatus(StatusEnum.IN_PROGRESS);
      setStartedAt(LocalDateTime.now());
    }}, new ChargingSession() {{
      setId(UUID.randomUUID());
      setStationId("STATION-2");
      setStatus(StatusEnum.FINISHED);
      setStartedAt(LocalDateTime.now().minusSeconds(20));
    }}, new ChargingSession() {{
      setId(UUID.randomUUID());
      setStationId("STATION-3");
      setStatus(StatusEnum.IN_PROGRESS);
      setStartedAt(LocalDateTime.now().minusSeconds(59));
    }});

    when(repository.getAll()).thenReturn(sessions);

    mvc.perform(get("/chargingSessions").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
            .andExpect(jsonPath("$.[0].stationId", is("STATION-1")))
            .andExpect(jsonPath("$.[0].status", is("IN_PROGRESS")))
            .andExpect(jsonPath("$.[1].stationId", is("STATION-2"))).andExpect(jsonPath("$.[1].status", is("FINISHED")))
            .andExpect(jsonPath("$.[2].stationId", is("STATION-3")))
            .andExpect(jsonPath("$.[2].status", is("IN_PROGRESS")));

    verify(repository, times(1)).getAll();
  }

  @Test
  public void testSummary() throws Exception {
    long inProgress = 3L;
    long finished = 4L;
    SessionSummary summary = new SessionSummary();
    summary.setStoppedCount(finished);
    summary.setStartedCount(inProgress);

    when(repository.countStarted(anyLong())).thenReturn(Integer.valueOf((int) inProgress));
    when(repository.countStopped(anyLong())).thenReturn(Integer.valueOf((int) finished));

    mvc.perform(get("/chargingSessions/summary").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
            .andExpect(jsonPath("$.totalCount", is((int) (inProgress + finished))))
            .andExpect(jsonPath("$.startedCount", is((int) inProgress)))
            .andExpect(jsonPath("$.stoppedCount", is((int) finished)));

    verify(repository, times(1)).countStarted(anyLong());
    verify(repository, times(1)).countStopped(anyLong());
  }
}
