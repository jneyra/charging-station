package everon.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import everon.model.ChargingSession;
import everon.model.ChargingSessionCreate;
import everon.model.StatusEnum;
import everon.repository.ChargingSessionRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ChargingSessionRestIntegrationTest {
  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  private MockMvc mvc;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private ChargingSessionRepository repository;

  private DateTimeFormatter format = DateTimeFormatter.ISO_DATE_TIME;

  @Test
  public void testInsert() throws Exception {
    mvc.perform(post("/chargingSessions").content(createSessionRequestBody(1)).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());

    assertEquals(1, repository.getAll().size());

    mvc.perform(post("/chargingSessions").content(createSessionRequestBody(2)).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());
    mvc.perform(post("/chargingSessions").content(createSessionRequestBody(3)).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());
    mvc.perform(post("/chargingSessions").content(createSessionRequestBody(4)).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());

    assertEquals(4, repository.getAll().size());
    assertEquals(4, repository.countStarted(0));
    assertEquals(0, repository.countStopped(0));
  }

  @Test
  public void testUpdate() throws Exception {
    mvc.perform(post("/chargingSessions").content(createSessionRequestBody(1)).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());

    List<ChargingSession> sessions = repository.getAll();
    assertEquals(1, sessions.size());

    ChargingSession firstCreated = sessions.get(0);
    mvc.perform(post("/chargingSessions").content(createSessionRequestBody(2)).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());
    mvc.perform(put("/chargingSessions/{id}", firstCreated.getId()).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isAccepted());

    sessions = repository.getAll();
    assertEquals(2, sessions.size());
    assertEquals(1, repository.countStarted(0));
    assertEquals(1, repository.countStopped(0));

    mvc.perform(post("/chargingSessions").content(createSessionRequestBody(3)).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());
    mvc.perform(post("/chargingSessions").content(createSessionRequestBody(4)).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());

    mvc.perform(put("/chargingSessions/{id}", firstCreated.getId()).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotModified());

    sessions = repository.getAll();
    assertEquals(4, sessions.size());
    assertEquals(3, repository.countStarted(0));
    assertEquals(1, repository.countStopped(0));
  }

  @Test
  public void testSessions_threading() throws Exception {
    int totalThreads = 10;
    int batchSize = 1000;
    for (int i = 0; i < 10; i++) {
      new Thread(() -> {
        try {
          createBatch(batchSize);
          logger.info("Finished " + Thread.currentThread().getName());
        } catch (Exception e) {
          e.printStackTrace();
        }
      }).start();

    }
    Thread.sleep(3_000L);
    assertEquals(totalThreads * batchSize, repository.countStarted(0));
    assertEquals(0, repository.countStopped(0));

    List<ChargingSession> items = repository.getAll();
    finishBatch(items.subList(0, 5));
    assertEquals((totalThreads * batchSize - 5), repository.countStarted(0));
    assertEquals(5, repository.countStopped(0));
  }

  @Test
  public void testSummary() throws Exception {
    createBatch(10);
    mvc.perform(get("/chargingSessions/summary").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.totalCount", is(10)))
            .andExpect(jsonPath("$.stoppedCount", is(0)))
            .andExpect(jsonPath("$.startedCount", is(10)));
    Thread.sleep(10_000L);

    List<ChargingSession> items = repository.getAll();
    finishBatch(items.subList(0, 5));
    mvc.perform(get("/chargingSessions/summary").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.totalCount", is(10)))
            .andExpect(jsonPath("$.stoppedCount", is(5)))
            .andExpect(jsonPath("$.startedCount", is(5)));

    createBatch(10);
    Thread.sleep(30_000L);

    items = repository.getAll();
    finishBatch(items.stream().sorted(this::compareSession).filter(e -> e.getStatus() == StatusEnum.IN_PROGRESS).collect(Collectors.toList()).subList(0, 5));
    mvc.perform(get("/chargingSessions/summary").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.totalCount", is(20)))
            .andExpect(jsonPath("$.stoppedCount", is(10)))
            .andExpect(jsonPath("$.startedCount", is(10)));

    createBatch(10);
    Thread.sleep(10_000L);
    items = repository.getAll();
    finishBatch(items.stream().sorted(this::compareSession).filter(e -> e.getStatus() == StatusEnum.IN_PROGRESS).collect(Collectors.toList()).subList(0, 5));
    mvc.perform(get("/chargingSessions/summary").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.totalCount", is(30)))
            .andExpect(jsonPath("$.stoppedCount", is(15)))
            .andExpect(jsonPath("$.startedCount", is(15)));

    Thread.sleep(10_000L);
    mvc.perform(get("/chargingSessions/summary").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.totalCount", is(20)))
            .andExpect(jsonPath("$.stoppedCount", is(5)))
            .andExpect(jsonPath("$.startedCount", is(15)));

  }

  int compareSession(ChargingSession cs1, ChargingSession cs2) {
    return cs1.getStartedAt().compareTo(cs2.getStartedAt());
  }

  private void createBatch(int batchSize) throws Exception {
    for (int i = 0; i < batchSize; i++) {
      ChargingSessionCreate create = chargingSessionCreate(i);
      String jsonBody = objectMapper.writeValueAsString(create);
      mvc.perform(post("/chargingSessions").content(jsonBody).contentType(MediaType.APPLICATION_JSON))
              .andExpect(status().isCreated());
    }
  }

  private ChargingSessionCreate chargingSessionCreate(int i) {
    ChargingSessionCreate session = new ChargingSessionCreate();
    session.setStationId("SESSION-" + i);
    session.setTimestamp(format.format(LocalDateTime.now()));
    return session;
  }

  private void finishBatch(List<ChargingSession> items) throws Exception {
    for (ChargingSession session : items) {
      mvc.perform(put("/chargingSessions/{id}", session.getId()).contentType(MediaType.APPLICATION_JSON))
              .andExpect(status().isAccepted());
    }
  }

  private String createSessionRequestBody(int i) throws JsonProcessingException {
    return objectMapper.writeValueAsString(chargingSessionCreate(i));
  }
}
