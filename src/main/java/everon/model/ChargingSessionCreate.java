package everon.model;

import java.util.UUID;

/**
 * Entity to capture ChargingSession creation used in REST API.
 */
public class ChargingSessionCreate {
  private UUID id;

  private String stationId;

  private String timestamp;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getStationId() {
    return stationId;
  }

  public void setStationId(String stationId) {
    this.stationId = stationId;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }
}
