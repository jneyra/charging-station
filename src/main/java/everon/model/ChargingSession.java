package everon.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Objects;
import java.util.UUID;

/**
 * Charging Session Entity
 */
public class ChargingSession {
  private UUID id;

  private String stationId;

  private LocalDateTime startedAt;

  private StatusEnum status;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getStationId() {
    return stationId;
  }

  public void setStationId(String stationId) {
    this.stationId = stationId;
  }

  public LocalDateTime getStartedAt() {
    return startedAt;
  }

  public void setStartedAt(LocalDateTime startedAt) {
    this.startedAt = startedAt;
  }

  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ChargingSession that = (ChargingSession) o;
    return id.equals(that.id) && Objects.equals(stationId, that.stationId) && Objects.equals(startedAt, that.startedAt)
            && status == that.status;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, stationId, startedAt, status);
  }

  @JsonIgnore
  public long getTimestamp() {
    return getStartedAt().toInstant(ZoneOffset.UTC).toEpochMilli();
  }

  public static ChargingSession copy(ChargingSession session) {
    if (session == null) {
      return null;
    }
    ChargingSession cloned = new ChargingSession();
    cloned.setId(session.getId());
    cloned.setStartedAt(LocalDateTime.from(session.getStartedAt()));
    cloned.setStationId(session.getStationId());
    cloned.setStatus(session.getStatus());
    return cloned;
  }

}
