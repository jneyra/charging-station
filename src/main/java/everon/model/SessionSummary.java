package everon.model;

/**
 * Represents the response for the session summary
 */
public class SessionSummary {

  private Long startedCount;

  private Long stoppedCount;

  public Long getTotalCount() {
    return startedCount + stoppedCount;
  }

  public Long getStartedCount() {
    return startedCount;
  }

  public void setStartedCount(Long startedCount) {
    this.startedCount = startedCount;
  }

  public Long getStoppedCount() {
    return stoppedCount;
  }

  public void setStoppedCount(Long stoppedCount) {
    this.stoppedCount = stoppedCount;
  }
}
