package everon.model;

public enum StatusEnum {
  IN_PROGRESS,
  FINISHED
}
