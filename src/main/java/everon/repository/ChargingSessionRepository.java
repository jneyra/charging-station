package everon.repository;

import everon.avltree.AVLTree;
import everon.model.ChargingSession;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/**
 * Repository to in the data-layer.
 * Normally it should access to a database. But in this case uses data-structures.
 */
@Component
public class ChargingSessionRepository {

  private Map<UUID, ChargingSession> data = new ConcurrentHashMap<>();

  private AVLTree<ChargingSessionIndex, UUID> startedIndex = new AVLTree<>();

  private AVLTree<ChargingSessionIndex, UUID> stoppedIndex = new AVLTree<>();

  private final Lock lock = new ReentrantLock();

  public ChargingSession insert(ChargingSession session) {
    ChargingSession newSession = ChargingSession.copy(session);
    newSession.setId(UUID.randomUUID());
    data.put(newSession.getId(), newSession);
    lock.lock();
    try {
      ChargingSessionIndex index = new ChargingSessionIndex(newSession.getTimestamp(), newSession.getId());
      startedIndex.insert(index, newSession.getId());
    } finally {
      lock.unlock();
    }
    return ChargingSession.copy(newSession);
  }

  public ChargingSession update(ChargingSession session) {
    if (session == null || session.getId() == null) {
      throw new RuntimeException("Cannot update sesion: " + session);
    }
    ChargingSession updatedSession = ChargingSession.copy(session);
    data.put(updatedSession.getId(), updatedSession);
    lock.lock();
    try {
      ChargingSessionIndex index = new ChargingSessionIndex(updatedSession.getTimestamp(), updatedSession.getId());
      startedIndex.remove(index);
      stoppedIndex.insert(index, updatedSession.getId());
    } finally {
      lock.unlock();
    }
    return ChargingSession.copy(updatedSession);
  }

  public ChargingSession getById(String id) {
    ChargingSession session = data.get(UUID.fromString(id));
    return ChargingSession.copy(session);
  }

  public List<ChargingSession> getAll() {
    return data.values().stream().map(ChargingSession::copy).collect(Collectors.toList());
  }

  public int countStarted(long since) {
    ChargingSessionIndex index = new ChargingSessionIndex(since, null);
    return startedIndex.countGreaterOrEquals(index);
  }

  public int countStopped(long since) {
    ChargingSessionIndex index = new ChargingSessionIndex(since, null);
    return stoppedIndex.countGreaterOrEquals(index);
  }
}
