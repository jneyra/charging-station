package everon.repository;

import java.util.Optional;
import java.util.UUID;

/**
 * Represents the ChargingSession Index to store in the AVL Tree.
 * This index allow to have session with same timestamp, avoiding override the data.
 */
public class ChargingSessionIndex implements Comparable<ChargingSessionIndex> {

  private Long timestamp;

  private Optional<UUID> uuid;

  public ChargingSessionIndex(Long timestamp, UUID uuid) {
    this.timestamp = timestamp;
    this.uuid = Optional.ofNullable(uuid);
  }

  @Override
  public int compareTo(ChargingSessionIndex o) {
    int result = timestamp.compareTo(o.timestamp);
    if (result != 0) {
      return result;
    }
    if (uuid.isPresent() && o.uuid.isPresent()) {
      return uuid.get().compareTo(o.uuid.get());
    } else if (uuid.isPresent()) {
      return 1;
    } else {
      return o.uuid.isPresent() ? -1 : 0;
    }
  }

  @Override
  public String toString() {
    return String.format("%s-%s", String.valueOf(timestamp), String.valueOf(uuid));
  }
}

