package everon.avltree;

/**
 * AVL Tree
 * @param <K>
 * @param <V>
 */
public class AVLTree<K extends Comparable<? super K>, V> {

  /**
   * Root node of the three
   */
  private AVLNode root;

  /**
   * Inserts node into where. The weight of the node is incremented in the place where will be inseted.
   * If the tree is not balanced. It is performed.
   * @param where
   * @param node
   * @return the inseted node
   */
  private AVLNode insert(AVLNode where, AVLNode node) {
    if (where == null) {
      return node;
    }
    where.weight++;
    int comp = where.key.compareTo(node.key);
    if (comp == 0) {
      where.value = node.value;
    } else if (comp < 0) {
      where.left = insert(where.left, node);
    } else {
      where.right = insert(where.right, node);
    }

    return where.balance();
  }

  /**
   * Removes a node based on a key.
   *
   * @param node
   * @param key
   * @return the removed node. If it's not found return null.
   */
  private AVLNode delete(AVLNode node, K key) {
    if (node == null) {
      return null;
    }

    int comp = node.key.compareTo(key);
    if (comp == 0) {
      if (node.left == null && node.right == null) {
        return null;
      } else if (node.left == null) {
        return node.right;
      } else if (node.right == null) {
        return node.left;
      } else {
        AVLNode left = node.right;
        while (left.left != null) {
          left = left.left;
        }

        left.right = deleteLeft(node.right);
        left.left = node.left;
        if (left.left != null) {
          left.left.recalculateWeight();
        }

        return left.balance();
      }
    } else if (comp < 0) {
      node.left = delete(node.left, key);
    } else {
      node.right = delete(node.right, key);
    }
    return node.balance();
  }

  private AVLNode deleteLeft(AVLNode node) {
    if (node.left == null) {
      return node.right;
    } else {
      node.left = deleteLeft(node.left);
      return node.balance();
    }
  }

  /**
   * Inserts a pair key-value into the tree.
   * If the key already exists, the value will be updated.
   * @param key
   * @param value
   */
  public void insert(K key, V value) {
    root = insert(root, new AVLNode(key, value));
  }

  /**
   * Removes a node based on the key.
   *
   * @param key
   */
  public void remove(K key) {
    root = delete(root, key);
  }

  /**
   * Counts the key's nodes greater or equals than key.
   *
   * @param key
   * @return
   */
  public int countGreaterOrEquals(K key) {
    return countGreaterOrEquals(root, key);
  }

  private int countGreaterOrEquals(AVLNode node, K key) {
    if (node == null) {
      return 0;
    }
    int total = 0;
    int comp = key.compareTo(node.key);
    if (comp > 0) {
      total += countGreaterOrEquals(node.left, key);
    } else if (comp <= 0) {
      total += countGreaterOrEquals(node.right, key) + weight(node.left) + 1;
    }
    return total;
  }

  int weight(AVLNode node) {
    return node == null ? 0 : node.weight;
  }


  public void clear() {
    root = null;
  }

  public int size() {
    return weight(root);
  }

  class AVLNode {
    private V value;

    private K key;

    AVLNode left;

    AVLNode right;

    int height;

    int weight;

    AVLNode(K key, V value) {
      this.key = key;
      this.value = value;
      this.height = 1;
      this.weight = 1;
    }

    AVLNode rotateRight() {
      AVLNode temp = this.left;

      this.left = temp.right;
      this.computeHeight();
      this.recalculateWeight();

      temp.right = this;
      temp.computeHeight();
      temp.recalculateWeight();

      return temp;
    }

    AVLNode rotateLeft() {
      AVLNode temp = this.right;

      this.right = temp.left;
      this.computeHeight();
      this.recalculateWeight();

      temp.left = this;
      temp.computeHeight();
      temp.recalculateWeight();

      return temp;
    }

    void computeHeight() {
      height = 1;
      if (left != null) {
        height += left.height;
      }
      if (right != null) {
        height += right.height;
      }
    }

    int getBalance() {
      int balance = 0;
      if (left != null) {
        balance += left.height;
      }
      if (right != null) {
        balance -= right.height;
      }

      return balance;
    }

    /**
     * Balances the node following the AVL strategies
     * @return
     */
    AVLNode balance() {
      computeHeight();
      int balance = getBalance();
      if (balance > 1) {
        if (left.getBalance() < 0) {
          left = left.rotateLeft();
        }
        return rotateRight();
      } else if (balance < -1) {
        if (right.getBalance() > 0) {
          right = right.rotateRight();
        }
        return rotateLeft();
      }

      recalculateWeight();
      return this;
    }

    /**
     * Recalculates the weight of the current node and its children.
     */
    private void recalculateWeight() {
      if (this.left != null) {
        calculateWeight(this.left);
      }
      if (this.right != null) {
        calculateWeight(this.right);
      }
      calculateWeight(this);
    }

    private void calculateWeight(AVLNode node) {
      node.weight = weight(node.left) + weight(node.right) + 1;
    }

  }
}

