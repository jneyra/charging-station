package everon.rest;

import everon.model.ChargingSession;
import everon.model.ChargingSessionCreate;
import everon.model.SessionSummary;
import everon.model.StatusEnum;
import everon.repository.ChargingSessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Implements End-Points
 */
@RestController
@RequestMapping("/chargingSessions")
public class ChargingSessionRest {

  private ChargingSessionRepository repository;

  @Autowired
  public ChargingSessionRest(ChargingSessionRepository repository) {
    this.repository = repository;
  }

  @PostMapping
  ResponseEntity<ChargingSessionCreate> handleNewSession(@RequestBody ChargingSessionCreate sessionCreate) {
    ChargingSession session = new ChargingSession();
    LocalDateTime startedAt = LocalDateTime.parse(sessionCreate.getTimestamp(), DateTimeFormatter.ISO_DATE_TIME);
    session.setStartedAt(startedAt);
    session.setStationId(sessionCreate.getStationId());
    session.setStatus(StatusEnum.IN_PROGRESS);
    ChargingSession created = repository.insert(session);
    sessionCreate.setId(created.getId());
    return new ResponseEntity<>(sessionCreate, HttpStatus.CREATED);
  }

  @PutMapping("/{id}")
  ResponseEntity<ChargingSession> handleUpdateSession(@PathVariable("id") String id) {
    ChargingSession session = repository.getById(id);
    if (session == null) {
      // Session doesn't exist
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    if (session.getStatus().equals(StatusEnum.FINISHED)) {
      // session already finished
      return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }
    session.setStatus(StatusEnum.FINISHED);
    return new ResponseEntity<>(repository.update(session), HttpStatus.ACCEPTED);
  }

  @GetMapping
  List<ChargingSession> handleListAll() {
    return repository.getAll();
  }

  @GetMapping("/summary")
  SessionSummary handleSummary() {
    SessionSummary summary = new SessionSummary();
    long since = LocalDateTime.now().minusMinutes(1L).toInstant(ZoneOffset.UTC).toEpochMilli();
    long inProgress = repository.countStarted(since);
    long finished = repository.countStopped(since);
    summary.setStartedCount(inProgress);
    summary.setStoppedCount(finished);
    return summary;
  }

}
